import React from "react";
import Telegram from "../SanberApp/Tugas/Tugas8/Telegram";
import LoginScreen from "./Tugas/Tugas9/LoginScreen";
import RegisterScreen from "./Tugas/Tugas9/RegisterScreen";
import AboutScreen from "./Tugas/Tugas9/AboutScreen";
import { NavigationContainer } from "@react-navigation/native";
import Router from "./Tugas/Tugas10/Router";

export default function App() {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}
