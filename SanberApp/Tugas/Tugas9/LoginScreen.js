import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { useState } from "react";

const colors = {
  lightBlue: "#3EC6FF",
  darkBlue: "#003366",
  grayBackground: "#EFEFEF",
};
export default function LoginScreen({ navigation }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <>
      <StatusBar
        style="dark"
        translucent={false}
        backgroundColor={colors.lightBlue}
      />
      <ScrollView style={styles.container}>
        <Image style={styles.headerIcon} source={require("./asset/Logo.png")} />
        <View style={styles.form}>
          <Text style={styles.title}>Login</Text>
          <Text style={styles.label}>Username</Text>
          <TextInput
            style={styles.inputText}
            onChangeText={setUsername}
            value={username}
          />
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.inputText}
            onChangeText={setPassword}
            value={password}
          />
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => navigation.navigate("myTab")}
            style={styles.buttonLogin}
          >
            <Text style={styles.buttonTextLogin}>Masuk</Text>
          </TouchableOpacity>
          <Text style={styles.footerText}>Atau</Text>
          <TouchableOpacity style={styles.buttonDaftar}>
            <Text style={styles.buttonTextDaftar}>Daftar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  headerIcon: {
    width: 300,
    height: 100,
    marginTop: 20,
    marginHorizontal: "auto",
    marginBottom: 10,
  },
  form: {
    margin: "auto",
    marginBottom: 25,
  },
  footer: {
    padding: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
    color: colors.darkBlue,
    marginBottom: 50,
  },
  label: {
    fontSize: 13,
    marginBottom: 5,
    fontWeight: "500",
  },
  inputText: {
    borderWidth: 1.5,
    borderColor: colors.darkBlue,
    height: 40,
    width: 250,
    marginBottom: 20,
    borderRadius: 5,
    padding: 10,
    fontSize: 13,
  },
  buttonLogin: {
    backgroundColor: colors.darkBlue,
    height: 45,
    width: 120,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "auto",
  },
  buttonTextLogin: {
    color: "white",
    fontSize: 20,
    fontWeight: "400",
    bottom: 2,
  },
  buttonDaftar: {
    backgroundColor: "white",
    height: 45,
    width: 120,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "auto",
    borderColor: colors.darkBlue,
    borderStyle: "solid",
    borderWidth: 1,
  },
  buttonTextDaftar: {
    color: colors.lightBlue,
    fontSize: 20,
    fontWeight: "400",
    bottom: 2,
  },
  footerText: {
    fontSize: 15,
    textAlign: "center",
    margin: 10,
    fontWeight: "400",
  },
});
