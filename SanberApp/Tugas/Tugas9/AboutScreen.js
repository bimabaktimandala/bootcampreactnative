import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Linking,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import Icon from "react-native-vector-icons/FontAwesome";
import Mail from "react-native-vector-icons/MaterialCommunityIcons";

const colors = {
  lightBlue: "#3EC6FF",
  darkBlue: "#003366",
  grayBackground: "#EFEFEF",
};

export default function AboutScreen() {
  return (
    <>
      <StatusBar
        style="light"
        translucent={false}
        backgroundColor={colors.lightBlue}
      />
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Tentang Saya</Text>
          <Icon
            style={styles.headerIcon}
            name="user-circle"
            size={100}
            color={colors.lightBlue}
          />
          <Text style={styles.headerText}>Mandala</Text>
          <Text style={styles.headerText}>React Native Developer</Text>
        </View>
        <View style={styles.card}>
          <Text style={styles.title}>Portofolio</Text>
          <View style={styles.line}></View>
          <View style={styles.iconContainer}>
            <View style={styles.textIcon}>
              <Icon
                style={styles.icon}
                name="gitlab"
                size={40}
                color="#FC6D26"
                onPress={() => {
                  Linking.openURL("https://gitlab.com/Mandala");
                }}
              />
              <Text style={styles.cap}>@mandala</Text>
            </View>
            <View style={styles.textIcon}>
              <Icon
                style={styles.icon}
                name="github"
                size={40}
                color="black"
                onPress={() => {
                  Linking.openURL("https://github.com/Mandala");
                }}
              />
              <Text style={styles.cap}>@mandala</Text>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <Text style={styles.title}>Contact</Text>
          <View style={styles.line}></View>
          <View style={styles.iconContainer}>
            <View style={styles.textIcon}>
              <Icon
                style={styles.icon}
                name="linkedin-square"
                size={40}
                color="#0077B5"
                onPress={() => {
                  Linking.openURL("https://www.linkedin.com/in/mandala/");
                }}
              />
              <Text style={styles.cap}>@mandala</Text>
            </View>
            <View style={styles.textIcon}>
              <Icon
                style={styles.icon}
                name="whatsapp"
                size={40}
                color="#128C7E"
                onPress={() => {
                  Linking.openURL("https://wa.me/6281234567890");
                }}
              />
              <Text style={styles.cap}>08123456789</Text>
            </View>
            <View style={styles.textIcon}>
              <Mail
                style={styles.icon}
                name="gmail"
                size={38}
                color="	#c71610"
                onPress={() => {
                  Linking.openURL("https://mail.google.com/mail/u/0/#inbox");
                }}
              />
              <Text style={styles.cap}>mandala@email.com</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    paddingTop: 30,
  },
  headerText: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  headerIcon: {
    width: 100,
    height: 100,
    margin: "auto",
    marginBottom: 10,
    marginHorizontal: "auto",
    marginTop: 20,
    marginBottom: 20,
  },
  card: {
    width: "80%",
    paddingVertical: 4,
    paddingHorizontal: 10,
    backgroundColor: colors.grayBackground,
    marginHorizontal: "auto",
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 20,
  },

  line: {
    borderWidth: 1,
    borderColor: "black",
  },
  title: {
    fontSize: 20,
    fontWeight: "400",
  },
  icon: {
    width: 40,
    height: 40,
  },
  iconContainer: {
    padding: 10,
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
  },
  textIcon: {
    alignItems: "center",
    width: 110,
  },
  cap: {
    fontSize: 12,
    textAlign: "center",
  },
});
