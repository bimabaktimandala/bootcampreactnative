import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { Data } from "./dummy";
const colors = {
  primary: "#517DA2",
  secondary: "#4EA4EA",
  caption: "#A8AAAB",
};

export default function Telegram() {
  return (
    <>
      <StatusBar
        style="light"
        translucent={false}
        backgroundColor={colors.primary}
      />
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.headerIcon}
            source={require("./asset/drawwer.png")}
          />
          <Text style={styles.titleName}>Telegram</Text>
          <Image
            style={styles.headerIcon}
            source={require("./asset/search.png")}
          />
        </View>

        <FlatList
          data={Data}
          renderItem={({ item }) => (
            <TouchableOpacity style={styles.messageItem}>
              <Image style={styles.messageAvatar} source={item.image} />
              <View style={styles.messageText}>
                <Text style={styles.messageTitle}>{item.name}</Text>
                <Text style={styles.messageContent}>{item.message}</Text>
              </View>
              <View style={styles.messageInfo}>
                <View style={styles.messageStatusTime}>
                  <Image
                    style={styles.messageStatus}
                    source={require("./asset/check.png")}
                  />
                  <Text style={styles.messageTime}>{item.time}</Text>
                </View>
                <Text style={styles.messageUnreadCounter}>
                  {item.totalMessage}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={() => <View style={styles.ItemSeparator} />}
        />
        <TouchableOpacity style={styles.fab}>
          <Image
            style={styles.fabIcon}
            source={require("./asset/pencil.png")}
          />
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: colors.primary,
    alignItems: "center",
    padding: 15,
    justifyContent: "space-between",
  },
  headerIcon: {
    width: 18,
    height: 12,
    resizeMode: "contain",
  },
  titleName: {
    flex: 1,
    color: "white",
    fontSize: 16,
    paddingLeft: 20,
  },
  messageItem: {
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingBottom: 5,
    paddingTop: 10,
  },
  messageAvatar: {
    width: 50,
    height: 50,
    borderRadius: 20,
  },
  messageText: {
    flex: 1,
    paddingHorizontal: 15,
  },
  messageTitle: {
    fontSize: 16,
    fontWeight: "600",
  },
  messageContent: {
    color: colors.caption,
  },
  messageInfo: {
    justifyContent: "space-around",
  },
  messageStatusTime: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  messageStatus: {
    width: 10,
    height: 10,
    resizeMode: "contain",
    marginRight: 5,
  },
  messageTime: {
    color: colors.caption,
    fontSize: 12,
  },
  messageUnreadCounter: {
    alignSelf: "flex-end",
    textAlign: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: colors.secondary,
    color: "white",
    borderRadius: 20,
    fontSize: 12,
  },
  ItemSeparator: {
    height: 1,
    backgroundColor: "#E5E5E5",
    width: "80%",
    alignSelf: "flex-end",
  },
  fab: {
    position: "absolute",
    right: 20,
    bottom: 20,
    backgroundColor: colors.secondary,
    borderRadius: 50,
    padding: 15,
  },
  fabIcon: {
    width: 20,
    height: 20,
    resizeMode: "contain",
  },
});
