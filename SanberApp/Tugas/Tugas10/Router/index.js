import React from "react";
import { StyleSheet, Text, View } from "react-native";
import LoginScreen from "../../Tugas9/LoginScreen";
import RegisterScreen from "../../Tugas9/RegisterScreen";
import AboutScreen from "../../Tugas9/AboutScreen";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

export default function Router() {
  const Stack = createNativeStackNavigator();
  const Tab = createBottomTabNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Register" component={RegisterScreen} />
      <Stack.Screen name="myTab" component={myTab} />
    </Stack.Navigator>
  );
}

function myTab() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  );
}
