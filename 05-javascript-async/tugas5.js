//soal 1
const myCountPromise = (input) => {
  return new Promise((resolve, reject) => {
    if (input !== undefined) {
      setTimeout(() => {
        resolve(input * 2);
      }, 1000);
    } else {
      reject("Parameter tidak boleh undefined");
    }
  });
};

//kode dibawah ini jangan dihapus atau diedit sama sekali ya
myCountPromise(2)
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });
